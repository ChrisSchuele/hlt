﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    public Store store;
    public Text headline;
    public Text leftText;
    public Text rightText;
    Data daten;
    PlayerData player = null;

    // Use this for initialization
    void Start()
    {
        store = GameObject.Find("Store").GetComponent<Store>();
        headline.text = "Kategorie: " + store.category + " \nLevel: " + store.level + "\nAnzahl Richtige: " + store.score;
        Load();
        foreach(PlayerData pd in daten.playerData)
        {
            if(pd.name == store.user)
            {
                SpielDaten newData = new SpielDaten();
                newData.mode = store.mode;
                newData.Punkte = store.score;
                newData.category = store.category;
                newData.dateTime = System.DateTime.Today;
                newData.level = store.level;
                pd.spielDaten.Add(newData);
                pd.level = store.level;
                Save();
                List<string> ergebnisse = new List<string>();
                List<string> ergebnisseMode = new List<string>();
                foreach (SpielDaten sd in pd.spielDaten)
                {
                    if (sd.mode == store.mode) {
                        ergebnisseMode.Add("Datum: " + String.Format("{0:yyyy-MM-dd}", sd.dateTime) + " Modus: " + sd.mode + " Punkte: " + sd.Punkte + " Level: " +sd.level);
                    }else
                    {
                        ergebnisse.Add("Datum: " + String.Format("{0:yyyy-MM-dd}", sd.dateTime) + " Modus: " + sd.mode + " Punkte: " + sd.Punkte + " Level: " + sd.level);
                    }
                }
                leftText.text = "";
                ergebnisseMode.Reverse();
                foreach (string text in ergebnisseMode)
                {
                    leftText.text += text + "\n";
                }

                rightText.text = "";
                ergebnisse.Reverse();
                foreach (string text in ergebnisse)
                {
                    rightText.text += text + "\n";
                }

            }
        }

 

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, daten);
        file.Close();
    }
    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            daten = (Data)bf.Deserialize(file);

            file.Close();
        }
        if (daten.playerData.Count == 0)
        {
            PlayerData newPlayer = new PlayerData("Standard Nutzer");
            daten.playerData.Add(newPlayer);
        }
    }
    public void exit()
    {
        store.LoadScene("MainMenu");
    }
}
