﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpielDaten {
    public int mode;
    public int category;
    public int Punkte;
    public System.DateTime dateTime;
    public int level;

}
