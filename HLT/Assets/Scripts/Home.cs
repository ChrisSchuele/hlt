﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class Home : MonoBehaviour
{
    public Data daten;
    public Dropdown nameDropdown;
    public Dropdown categoryDropdown;
    public Dropdown modeDropdown;
    public Store store;
    public InputField inputFieldNewUser;
    // Use this for initialization
    void Start()
    {
        store = GameObject.Find("Store").GetComponent<Store>();
        store.score = 0;
        store.round = 0;
        Load();
        reloadNameDropdown();
        store.score = 0;
        store.round = 0;
        modeDropdown.value = daten.playerData[daten.playerData.Count - 1].spielDaten[daten.playerData[daten.playerData.Count - 1].spielDaten.Count - 1].mode;
        categoryDropdown.value = daten.playerData[daten.playerData.Count - 1].spielDaten[daten.playerData[daten.playerData.Count - 1].spielDaten.Count - 1].category;
        int x = 0;
       foreach(Dropdown.OptionData od in nameDropdown.options)
        {
            if(od.text == daten.playerData[daten.playerData.Count - 1].name)
            {
                nameDropdown.value = x;
            }
            x++;
        }
        newMode();
    

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void reloadNameDropdown()
    {
        
        List<string> options = new List<string>();
        foreach (PlayerData pd in daten.playerData)
        {
            options.Add(pd.name);
        
        }
        nameDropdown.ClearOptions();
        nameDropdown.AddOptions(options);
        nameDropdown.value = 0;
        store.user = nameDropdown.GetComponentInChildren<Text>().text;
    }

    public void newMode()
    {
        store.category = categoryDropdown.value;
        store.mode = modeDropdown.value;
        store.user = nameDropdown.GetComponentInChildren<Text>().text;
        foreach (PlayerData pd in daten.playerData)
        {
   
            if (pd.name.Equals(store.user))
            {
                store.level = pd.level;
               
            }
        }
    }

    public void newUser()
    {

        if (inputFieldNewUser.text != "")
        {
            PlayerData newPlayer = new PlayerData(inputFieldNewUser.text);
            daten.playerData.Add(newPlayer);
            Save();
            reloadNameDropdown();
            nameDropdown.value = nameDropdown.options.Count - 1;
        }
    }
    public void deleteUser()
    {
        if(nameDropdown.GetComponentInChildren<Text>().text == "Standard Nutzer")
        {
            return;
        }
        foreach (PlayerData pd in daten.playerData)
        {
           
            if (pd.name.Equals(nameDropdown.GetComponentInChildren<Text>().text))
            {
                daten.playerData.Remove(pd);
                Save();
                reloadNameDropdown();

                return;
            }
        }
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, daten);
        file.Close();
    }
    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            daten = (Data)bf.Deserialize(file);

            file.Close();
        }
        if(daten.playerData.Count == 0)
        {
            PlayerData newPlayer = new PlayerData("Standard Nutzer");
            daten.playerData.Add(newPlayer);
        }
    }
}
