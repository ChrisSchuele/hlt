﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoMenu : MonoBehaviour {
    public UnityEngine.UI.Text text;
    public Store store;
	// Use this for initialization
	void Start () {
        store = GameObject.Find("Store").GetComponent<Store>();
        switch (store.mode)
        {
            case 0: text.text = "Im ersten Schritt werden bis zu 8 Bilder aus der gewählten Kategorie angezeigt. Die Aufgabe ist es, sich die Zahlen zu den Bildern zu merken.Nach Drücken des Weiter-Buttons wird ein Bild eingeblendet und die jeweilige Zahl muss ausgewählt werden";
            break;
            case 1:
                text.text = "Im ersten Schritt werden bis zu 8 Bilder aus der gewählten Kategorie angezeigt. Die Aufgabe ist es, sich die Zahlen zu den Bildern zu merken. Dabei wird ein Countdown von 10 Sekunden heruntergezählt, bis ein Bild eingeblendet wird und die dazugehörige Zahl ausgewählt werden muss.";
                break;
            case 2:
                text.text = "Im ersten Schritt werden bis zu 8 Bilder aus der gewählten Kategorie angezeigt. Die Aufgabe ist es, sich die Zahlen zu den Bildern zu merken. Nach Drücken des Weiter-Buttons wird ein Bild eingeblendet und die jeweilige Zahl muss ausgewählt werden. Als Besonderheit kann es vorkommen, dass ein Bild angezeigt wird, welches in der ersten Auswahl NICHT vorkam. Dann muss der Button 'unbekanntes Bild' ausgewählt werden";
                break;
            case 3:
                text.text = "Im ersten Schritt werden bis zu 8 Bilder aus der gewählten Kategorie angezeigt. Die Aufgabe ist es, sich die Zahlen zu den Bildern zu merken. Nach Drücken des Weiter-Buttons wird ein Bild eingeblendet und die jeweilige Zahl muss ausgewählt werden. Als Besonderheit sind die Zahlen unter den Bildern nicht der Reihe nach durchnummeriert sondern durcheinander";
                break;




        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void LoadScene(string name)
    {
        store.LoadScene(name);
    }
}
