﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {
    public string name = "";
    public List<SpielDaten> spielDaten = new List<SpielDaten>();
    public int level = 4;


    public PlayerData(string name)
    {
        this.name = name;
         level = 4;
}
}
