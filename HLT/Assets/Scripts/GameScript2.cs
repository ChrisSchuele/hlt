﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScript2 : MonoBehaviour {

    public Store store;
    public GameObject image;
    public Button[] buttons;
    int rnd = 0;
    bool clicked = false;
    public Text headline;
    public Text infoText;
    // Use this for initialization
    void Start()
    {
        store = GameObject.Find("Store").GetComponent<Store>();
        headline.text = "Punktestand: " + store.score;

            int anzBilder = store.level / 2;
        if (anzBilder < 2)
        {
            anzBilder = 2;
        }
        if (anzBilder > 8)
        {
            anzBilder = 8;
        }

        rnd = UnityEngine.Random.Range(0, anzBilder);
       
        image.GetComponent<Image>().sprite = store.getImage(rnd);
        if (store.mode == 2)
        {
            int rnd2 = UnityEngine.Random.Range(0, 5);
            if (rnd2 == 0)
            {
                image.GetComponent<Image>().sprite = store.getImage(8);
                rnd = 8;
            }
            buttons[8].gameObject.SetActive(true);
           

        }
        infoText.text = "Spieler: " + store.user + "\nSpielerlevel: " + store.level;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void buttonClick(int nr)
    {
        if (clicked)
            return;
        clicked = true;
        store.round++;
        if (nr == store.numbers[rnd])
        {
            store.score++;
            store.correctCount++;
            headline.text = "Punktestand: " + store.score;
            if(store.level < 20)
            store.level++;
        }else
        {
            if (store.level > 0)
                store.level--;
        }
        buttons[nr-1].GetComponent<Image>().color = Color.red;
        buttons[store.numbers[rnd]-1].GetComponent<Image>().color = Color.green;
        StartCoroutine(loadNextScene());

    }

    IEnumerator loadNextScene()
    {
        yield return new WaitForSeconds(1.5f);
        if (store.round >= store.gameLength)
            store.LoadScene("EndScreen");
        else store.LoadScene("Game");
      
    }
    public void exit()
    {
        store.LoadScene("MainMenu");
    }
}
