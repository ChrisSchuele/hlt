﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScript : MonoBehaviour {

    public Store store;
    public GameObject[] images;
    public Text headline;
    public Button buttonNext;
    public Text infoText;
    // Use this for initialization
    void Start()
    {
        store = GameObject.Find("Store").GetComponent<Store>();
        store.suffelCategory();
        headline.text = "Runde " + (store.round +1) + " von " + store.gameLength;
        for (int i = 0; i < 8; i++)
        {
            images[i].GetComponent<Image>().sprite = store.getImage(i);
            images[i].GetComponentInChildren<Text>().text = store.numbers[i] + "";
        }
        int anzBilder = store.level / 2;
        if (anzBilder < 2)
        {
            anzBilder = 2;
        }
        if (anzBilder > 8)
        {
            anzBilder = 8;
        }

        for (int i = anzBilder; i < 8; i++)
        {
            images[i].SetActive(false);
        }
        if(store.mode == 1)
        {
            StartCoroutine(loadNextScene(10));      
        }
        infoText.text = "Spieler: " + store.user + "\nSpielerlevel: " + store.level;
    }

    IEnumerator loadNextScene(int x)
    {
        buttonNext.GetComponentInChildren<Text>().text = "Weiter in " + x;
        yield return new WaitForSeconds(1f);
        if (x == 0)
        {
            store.LoadScene("Game2");
        }
        else
        {
            StartCoroutine(loadNextScene(x-1));
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
    public void exit()
    {
        store.LoadScene("MainMenu");
    }
    public void weiter()
    {
        store.LoadScene("Game2");
    }
}
