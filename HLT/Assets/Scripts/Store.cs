﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Store : MonoBehaviour {

    private bool created = false;
    public int level = 0;
    public int category = 0;
    public int mode = 0;
    public string user = "-1";
    public int correctCount = 0;
    public int round = 0;
    public int gameLength = 2;
    public int score = 0;
    public Sprite[] category0;
    public Sprite[] category1;
    public Sprite[] category2;
    public Sprite[] category3;
    public Sprite[] category4;
    public Sprite[] category5;
    public Sprite[] category6;
    public Sprite[] category7;
    public Sprite[] category8;
    public Sprite[][] images = new Sprite[9][];
    public int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9};
    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            Debug.Log("Awake: " + this.gameObject);
            images[0] = category0;
            category0 = null;
            images[1] = category1;
            category1 = null;
            images[2] = category2;
            category2 = null;
            images[3] = category3;
            category3 = null;
            images[4] = category4;
            category4 = null;
            images[5] = category5;
            category5 = null;
            images[6] = category6;
            category6 = null;
            images[7] = category7;
            category7 = null;
            images[8] = category8;
            category8 = null;
        }

    }

    public Sprite getImage(int i)
    {
        if (i < images[category].Length)
            return images[category][i];
        else
        {
            return images[category][0];
        }
    }
    public void LoadScene(string sceneName)
    {

        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);

    }
    public void suffelCategory()
    {
        if (category < images.Length && category >= 0)
        {
            Sprite tmp;
            for (int p = 0; p < 5; p++)
            {
                for (int i = 0; i < images[category].Length; i++)
                {
                    int rnd = UnityEngine.Random.Range(0, images[category].Length);
                    tmp = images[category][rnd];
                    images[category][rnd] = images[category][i];
                    images[category][i] = tmp;
                }
            }
        }
        if(mode == 3)
        {
            int tmp;
            for (int p = 0; p < 5; p++)
            {
                for (int i = 0; i < numbers.Length-1; i++)
                {
                    int rnd = UnityEngine.Random.Range(0, numbers.Length-1);
                    tmp = numbers[rnd];
                    numbers[rnd] = numbers[i];
                    numbers[i] = tmp;
                }
            }
        }else
        {
            Array.Sort(numbers);
        }
    }
    
}
